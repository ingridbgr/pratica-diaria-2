const defaultState = [
  {
    id: 1,
    name: "Maça",
    type: "fruit",
    image:
      "https://www.paulistaflores.com.br/image/cache/data/produtos/monte-sua-cesta/frutas/maca01---maca/maca-gala-1-926x926.jpg",
  },

  {
    id: 2,
    name: "Abacaxi",
    type: "fruit",
    image:
      "https://cdn.awsli.com.br/600x700/305/305913/produto/10189815/abacaxi-1441a8b7.jpg",
  },

  {
    id: 3,
    name: "Macarrão",
    type: "carb",
    image:
      "https://static1.casapraticaqualita.com.br/articles/3/22/33/@/2542-uma-boa-dica-de-macarrao-na-panela-de-pr-article_content_img-3.jpg",
  },

  {
    id: 4,
    name: "Tomate",
    type: "fruit",
    image:
      "https://www.infoescola.com/wp-content/uploads/2011/01/tomate_345187874.jpg",
  },

  {
    id: 5,
    name: "Bife",
    type: "meat",
    image: "https://guiadossolteiros.com/wp-content/uploads/2010/12/bife.jpg",
  },

  {
    id: 6,
    name: "Frango",
    type: "meat",
    image:
      "https://receitinhas.s3-sa-east-1.amazonaws.com/wp-content/uploads/2017/09/frango-assado-com-mostarda-e-mel-848x477.jpg",
  },

  {
    id: 7,
    name: "Pepsi",
    type: "carb",
    image:
      "https://carrefourbr.vtexassets.com/arquivos/ids/207442-1200-auto?width=1200&height=auto&aspect=true",
  },

  {
    id: 8,
    name: "Açucar",
    type: "carb",
    image:
      "https://a-static.mlcdn.com.br/618x463/acucar-confeiteiro-glacucar-500g-ext-fino-uniao/lojanovamix/1105/51262526f84735eafd282fc14df3df28.jpg",
  },

  {
    id: 9,
    name: "Chocolate",
    type: "carb",
    image:
      "https://saude.abril.com.br/wp-content/uploads/2016/11/chocolate01.jpg?quality=85&strip=info&resize=680,453",
  },
];

const productsReducer = (state = defaultState, action) => {
  return state;
};

export default productsReducer;
