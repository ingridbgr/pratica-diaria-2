const cartReducer = (state = [], action) => {
  switch (action.type) {
    case "@cart/ADD":
      const { product } = action;
      return [...state, product];

    case "@cart/REMOVE":
      const newList = state.filter((product) => product.id !== action.id);
      return newList;

    case "@cart/EMPTY":
      const emptyCard = "seu carrinho está vazio ";
      return emptyCard;

    default:
      return state;
  }
};

export default cartReducer;
