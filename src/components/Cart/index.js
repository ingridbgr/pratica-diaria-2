import { useSelector } from "react-redux";
import Product from "../Product";

const Cart = () => {
  const cart = useSelector((state) => state.cart);
  console.log(cart);

  return (
    <div>
      <h1>Meu carrinho de compras</h1>
      {cart.length === 0 ? (
        <p>seu carrinho está vazio </p>
      ) : cart.find((product) => product.type === "fruit" && "meat") ? (
        <h2> Parabéns, Sua compra está saudável!</h2>
      ) : (
        <h2> Cuidado, muito açucar na dieta é prejudicial a saúde</h2>
      )}

      {cart.map((product, index) => (
        <Product key={index} product={product} isRemovable />
      ))}

      <p>
        <label>Total de produtos= </label>
        <label>{cart.reduce((acc, product) => acc + 1, 0)}</label>
      </p>
    </div>
  );
};

export default Cart;
