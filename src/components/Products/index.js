import { useSelector } from "react-redux";
import Product from "../Product";

const Products = () => {
  const products = useSelector((state) => state.products);
  console.log(products);

  return (
    <div>
      {products.map((product, index) => (
        <Product key={index} product={product} />
      ))}
    </div>
  );
};

export default Products;
