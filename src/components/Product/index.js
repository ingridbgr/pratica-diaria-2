import { useDispatch } from "react-redux";
import { addToCart, removeFromCart } from "../../store/modules/cart/actions";

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    flexGrow: 1,
  },

  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  bullet: {
    display: "inline-block",
    margin: "4px",
    transform: "scale(0.3)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 5,
  },
  media: {
    height: 240,
  },
}));
const Product = ({ product, isRemovable = false }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <Paper className={classes.paper}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={product.image}
            title={product.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {product.name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {product.type}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Paper>
      <CardActions>
        {isRemovable ? (
          <Button onClick={() => dispatch(removeFromCart(product.id))}>
            {" "}
            Remover do carrinho
          </Button>
        ) : (
          <Button onClick={() => dispatch(addToCart(product))}>
            {" "}
            adicionar ao carrinho
          </Button>
        )}
      </CardActions>
    </Card>
  );
};

export default Product;
